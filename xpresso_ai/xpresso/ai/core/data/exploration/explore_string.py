__all__ = ["ExploreString"]
__author__ = ["Srijan Sharma"]

class ExploreString():
    def __init__(self,data):
        self.data = data
        return

    def populate_string(self):
        resp = dict()
        freq_count = self.string_analysis(self.data)
        resp["freq_count"] = freq_count
        resp["tobedone"] = "Exploration of string type data"
        return resp

    @staticmethod
    def string_analysis(data):
        freq_count = data.dropna().value_counts().to_dict()

        return freq_count